var express = require('express');
var usersFile = require('./users.json');
var bodyParser = require('body-parser');

var app = express();

var URL_BASE = '/apiperu/v1/';

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())

//GET USERS
app.get(URL_BASE + 'users', function(req, res) {
  console.log(req.query);

  res.status(200).send(usersFile);
});

//GET USER
app.get(URL_BASE + 'users/:id', function(req, res) {
  for (var i = 0; i < usersFile.length; i++) {
      if (usersFile[i].userid == req.params.id) {
          var user = usersFile[i];
          res.status(200).send(user);
      }
  }

  res.status(404).send({ code: 404, message: "El usuario no existe"});
});

//POST USER
app.post(URL_BASE + 'users', function(req, res) {
  var user = req.body;

  let newId = usersFile.length + 1;
  let newUser =  {
    userId: newId,
    firstName: user.firstName,
    lastName: user.lastName,
    email: user.email,
    password: user.password
  };

  usersFile.push(newUser);

  res.status(201).send(newUser);
});

//PUT USER
app.put(URL_BASE + 'users/:id', function(req, res) {
  var user = req.body;

  for (var i = 0; i < usersFile.length; i++) {
      if (usersFile[i].userid == req.params.id) {
          var userBd = usersFile[i];
          userBd.firstName = user.firstName || userBd.firstName;
          userBd.lastName = user.lastName || userBd.lastName;
          userBd.email = user.email || userBd.email;
          userBd.password = user.password || userBd.password;

          res.status(200).send(userBd);
      }
  }

  res.status(404).send({ code: 404, message: "El usuario no existe"});
});

//GET USER
app.delete(URL_BASE + 'users/:id', function(req, res) {
  for (var i = 0; i < usersFile.length; i++) {
      if (usersFile[i].userid == req.params.id) {
          usersFile.splice(i, 1);

          res.status(200).send({ code: 200, message: "El usuario ha sido eliminado" });
      }
  }

  res.status(404).send({ code: 404, message: "El usuario no existe"});
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
