var express = require('express');
var usersFile = require('./users.json');
var bodyParser = require('body-parser');
var requestJson = require('request-json');

var app = express();

var URL_BASE = '/apiperu/v1/';

var URL_BASE_MLAB = 'https://api.mlab.com/api/1/databases/mongobbvaperu/collections/';
var API_KEY_MLAB = 'apiKey=VgyjsQ2Cfxq6JH9lsGzKMXe6eDXLlOh3'

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())

//LOGIN USER
app.post(URL_BASE + 'login', function(req, res) {
    var email = req.headers.email;
    var password = req.headers.password;

    var httpClient = requestJson.createClient(URL_BASE_MLAB);
    var query = 'q={"email":"' + email + '","password":"' + password + '"}';

    httpClient.get('user?' + query + "&l=1&" + API_KEY_MLAB, function(err, resM, bodyMlab) {
      if (!err) {
        if (bodyMlab && bodyMlab.length > 0) {
          var cambio = '{"$set":{"logged":true}}';

          httpClient.put('user?q={"userid": ' + bodyMlab[0].userid + '}&' + API_KEY_MLAB, JSON.parse(cambio), function(errP, resP, bodyP) {
              res.status(200).send({"login":"ok", "userid":bodyMlab[0].userid, "nombre":bodyMlab[0].firstName, "apellidos":bodyMlab[0].lastName});
          });
        } else {
          res.status(404).send({ code: 404, message: "El usuario no existe"});
        }
      }
    });
})

//LOGOUT USER
app.post(URL_BASE + 'logout', function(req, res) {
    var userid = req.headers.userid;

    var httpClient = requestJson.createClient(URL_BASE_MLAB);
    var query = 'q={"userid":' + userid + ', "logged":true}';

    httpClient.get('user?' + query + "&l=1&" + API_KEY_MLAB, function(err, resM, bodyMlab) {
      if (!err) {
        if (bodyMlab && bodyMlab.length > 0) {
          var cambio = '{"$set":{"logged":false}}';

          httpClient.put('user?q={"userid": ' + bodyMlab[0].userid + '}&' + API_KEY_MLAB, JSON.parse(cambio), function(errP, resP, bodyP) {
              res.status(200).send({"logout":"ok", "userid":bodyMlab[0].userid});
          });
        } else {
          res.status(200).send({ code: 200, message: "El usuario no se encuentra logueado"});
        }
      }
    })
});

//GET USERS
app.get(URL_BASE + 'users', function(req, res) {
  var httpClient = requestJson.createClient(URL_BASE_MLAB);
  var querySring = '&f={"_id":0}&';

  httpClient.get('user?' + req.query + querySring + API_KEY_MLAB, function(errMlab, reqMlab, bodyMlab) {
      if(errMlab) {
        res.status(503).send({ code: 503, message: "Servicio no disponible"});
      }

      res.status(200).send(bodyMlab);
  });
});

//GET USER
app.get(URL_BASE + 'users/:id', function(req, res) {
  var httpClient = requestJson.createClient(URL_BASE_MLAB);
  var querySring = '&f={"_id":0}&';

  httpClient.get('user?q={"userid":' + req.params.id + '}' + querySring + API_KEY_MLAB, function(errMlab, reqMlab, bodyMlab) {
      if(errMlab) {
        res.status(503).send({ code: 503, message: "Servicio no disponible"});
      }

      if (bodyMlab && bodyMlab.length > 0) {
        res.status(200).send(bodyMlab[0]);
      } else {
        res.status(404).send({ code: 404, message: "El usuario no existe"});
      }
  });
});

//POST USER
app.post(URL_BASE + 'users', function(req, res) {
  var user = req.body;

  let newId = usersFile.length + 1;
  let newUser =  {
    userid: newId,
    firstName: user.firstName,
    lastName: user.lastName,
    email: user.email,
    password: user.password
  };

  var httpClient = requestJson.createClient(URL_BASE_MLAB);
  var querySring = '&f={"_id":0}&';

  httpClient.post('user?' + API_KEY_MLAB, newUser, function(errMlab, reqMlab, bodyMlab) {
      if(errMlab) {
        res.status(503).send({ code: 503, message: "Servicio no disponible"});
      }

      res.status(201).send(newUser);
  });
});

//PUT USER
app.put(URL_BASE + 'users/:id', function(req, res) {
  var user = req.body;

  var httpClient = requestJson.createClient(URL_BASE_MLAB);

  var userUpdate = '{"$set":' + JSON.stringify(user) + '}';

  httpClient.put('user?q={"userid":' + req.params.id + '}&' + API_KEY_MLAB, JSON.parse(userUpdate), function(errMlab, reqMlab, bodyMlab) {
      if(errMlab) {
        res.status(503).send({ code: 503, message: "Servicio no disponible"});
      }

      if (bodyMlab && bodyMlab.n > 0) {
        res.status(200).send({ code: 200, message: "El usuario ha sido modificado"});
      } else {
        res.status(404).send({ code: 404, message: "El usuario no existe"});
      }
  });
});

//GET USER
app.delete(URL_BASE + 'users/:id', function(req, res) {
  var httpClient = requestJson.createClient(URL_BASE_MLAB);

  httpClient.put('user?q={"userid":' + req.params.id + '}&' + API_KEY_MLAB, {}, function(errMlab, reqMlab, bodyMlab) {
      if(errMlab) {
        res.status(503).send({ code: 503, message: "Servicio no disponible"});
      }

      if (bodyMlab && bodyMlab.n > 0) {
        res.status(200).send({ code: 200, message: "El usuario ha sido eliminado"});
      } else {
        res.status(404).send({ code: 404, message: "El usuario no existe"});
      }
  });
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
